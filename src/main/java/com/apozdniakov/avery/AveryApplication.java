package com.apozdniakov.avery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(AveryApplication.class, args);
    }

}
